const express = require("express");

const mongoose = require("mongoose")

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://sepienkhel19:admin123@zuitt-bootcamp.bnuv3za.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const taskSchema = new mongoose.Schema({
	username: String,
	password: String,
	status:{
		type: String,
		default: "Pending"
	}
});

const Task = mongoose.model("Task", taskSchema);

app.post("/signup", (req, res) => {
Task.findOne({name: req.body.name}).then((result, err) => {

		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task ({
				name: req.body.name
			});
			newTask.save().then((saveTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered");
				}
			})
		}
	})
});
app.listen(port, () => console.log(`Server running at port ${port}`));